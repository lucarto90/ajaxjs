document.getElementById('cargar').addEventListener('click', cargarDatos);

function cargarDatos () {
    // crear el objeto XMLHttpRequest
    const xhr = new XMLHttpRequest();

    // abrir conexion
    xhr.open('GET', 'datos.txt', true);

    // una vez carga en la version antigua
    xhr.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            document.getElementById('listado').innerHTML = `<h1>${this.responseText}</h1>`;
        }
    }

    // ready status
    /*
    * 0 - no inicializado
    * 1 - conexion establecida
    * 2 - recibido
    * 3 - procesando
    * 4 - respuesta lista
    */ 


    // una vez carga, en la nueva version
    /* xhr.onload = function() {
        // 200: correcto, 403: prohibido, 404: no encontrado
        if (this.status === 200) {
            document.getElementById('listado').innerHTML = `<h1>${this.responseText}</h1>`;
        }
    }*/

    // enviar el request
    xhr.send();
}